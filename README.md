# Tiny Twenty

**Tiny Twenty** (ou **t20**) est une version modifiée et personnalisée de **Microlite20**. 

## Introduction

### Introduction de Microlite20

Ceci est une version allégée et réduite des règles DRS du plus populaire des JDR de fantasy (voir la licence pour plus d’information) ; elle a été conçue pour être rapide et facile à jouer.

Le but était en effet de créer un jeu plus accessible, mais où toutes les ressources du JDR dont il est issu (monstres, sorts, aventures et équipement) pourraient être utilisées sans effort de conversion.

### Introduction de t20

J'ai souhaité créer **Tiny Twenty** parce que j'aimais beaucoup l'idée d'avoir une version allégée de D&D, mais j'ai changé les points de règles que je trouvais trop compliqués/abstraits. J'ai aussi changé la compatibilité avec D&D et j'ai utilisé la 5E édition plutôt que la 3.5. Enfin, j'ai (ré)écrit certains points de règles afin que les joueurs aient une base claire sur laquelle se reposer.

L'objectif de **t20** est d'avoir un JDR simple et fun pour des parties héroïques : les aventuriers sont plus des héros que de simples baroudeurs. Mais comme tous les héros, ils vont devoir braver les épreuves et les dangers pour arriver à la victoire.

## Règles pour les joueurs•euses

Si vous êtes un joueur et que vous souhaitez commencer, le mieux est de regarder [comment créer son personnage](./joueur/creation-personnage.md). Vous pouvez aussi en choisir un (ou vous inspirer) dans la [liste des personnages pré-tirés](./joueur/personnages-pretires.md).

Voilà la liste des règles disponibles :

- [Création de personnage](./joueur/creation-personnage.md)
- [Classes](./joueur/classes.md)
- [Races](./joueur/races.md)
- [Magie](./magie/magie.md) et ses spécialités : [divine](./magie/divine.md), [druidique](./magie/druidique.md), [illusion](./magie/illusion.md) et [profane](./magie/profane.md)
- [Partir à l'aventure](./regles/aventure.md)
- [Combattre](./regles/combat.md)

Si vous vous intéressez aux équipements, regardez ici :

- [Armes](./equipement/armes.md)
- [Armures](./equipement/armures.md)
- [Montures](./equipement/montures.md)
- [Objets](./equipement/objets.md)
- [Outils](./equipement/outils.md)

*À noter* : une pièce d’or (PO) vaut 10 pièces d’argent (PA) ; et chaque pièce d’argent vaut 10 pièces de cuivre (PC).

## Règles pour les *Maître•sse•s du Jeu*

Partie en cours de rédaction...

## Remerciements

Parce que **t20** n'existerai pas sans eux, je tiens à remercier grandement l'équipe de [RedNet.io](https://rednet.io) avec une mention spéciale pour *Pierre Beaujeu* et *Jean Dupouy*. Je remercie également énormément [Raphaël Yan](https://raphaelyan.github.io/) pour son support inconditionnel. 

## Inspirations et ressources

Je remercie aussi toutes les personnes ayant travaillé sur [Microlite20](https://microlite20.org/), ainsi que ses dérivés (surtout [Microlite2020](https://www.drivethrurpg.com/product/308013/Microlite2020-Complete) et [Microluxe20](https://microluxe20.com/)), sans oublier [Le scriptorium](https://www.scriptorium.d100.fr/) pour les traductions en français. 

Comme **t20** a été revu et corrigé à la sauce D&D 5E, je remercie aussi les ressources du site [AideDD](https://www.aidedd.org/) qui est toujours d'une grande aide.
