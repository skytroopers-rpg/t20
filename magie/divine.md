# Magie divine

Voilà la liste des sorts divins disponibles :

## Sorts divins de niveau 0 

**Assistance divine** : +1 sur un jet d’attaque, de sauvegarde ou de compétence pendant 1 minute/niveau ou jusqu’à utilisation.
**Création d’eau** : crée 8 litres d’eau pure/niveau. 
**Lumière** : fait briller un objet comme une torche pendant 10 minutes/niveau.
**Purification de nourriture et d’eau** : purifie 30 L de nourriture et d’eau/niveau.
**Résistance** : confère +1 aux jets de sauvegarde pendant 1 minute.
**Stimulant** : confère 1 PV temporaire à la cible pendant 1 minute.

## Sorts divins de niveau 1

**Bénédiction** : les alliés gagnent +1 à l’attaque et aux jets de Communication + ESP pendant 1 minute/niveau.
**Bénédiction de l’eau** : crée de l’eau bénite. 
**Bouclier de la foi** : aura conférant un bonus à la CA de +2 ou plus pendant 1 minute/niveau. 
**Faveur divine** : confère un bonus de +1/3 niveaux aux jets d’attaque et de dégâts (min 1).
**Pierre magique** : 3 projectiles ; +1 à l’attaque, `1d6 + 1` points de dégâts. Dure 30 minutes ou jusqu’à utilisation.
**Soins légers** : rend 1d8 PV au sujet, +1/niveau (max. +5).

## Sorts divins de niveau 2

**Aide** : +1 aux jets d’attaque et de sauvegarde contre la terreur, 1d8 PV temporaires, +1/niveau (max. +10).
**Délivrance de la paralysie** : délivre une créature ou plus de paralysie, d’immobilisation et de lenteur.
**Préservation des morts** : préserve un cadavre. 
**Ralentissement du poison** : neutralise le poison pendant 1 heure/niveau.
**Restauration partielle** : dissipe effets magiques affaiblissants ou rend 1d4 points de caractéristique perdus.
**Soins modérés** : rend 2d8 PV au sujet, +1/niveau (max. +10).

## Sorts divins de niveau 3

**Création de nourriture et d’eau** : nourrit 3 humains ou 1 cheval/niveau.
**Guérison des maladies** : guérit tous les maux du sujet.
**Lumière brûlante** : 1d8 points de dégâts/2 niveaux ou 1d8 dégâts/niveau contre les morts-vivants.
**Prière** : +1 pour les alliés à presque tous les jets, –1 pour les adversaires pendant 1 round/niveau.
**Soins importants** : rend `3d8` PV à la cible, +1/niveau (max. +15).
**Communicationaveclesmorts** :uncadavrerépond à 1 question/2 niveaux pendant 1 minute/niveau.

## Sorts divins de niveau 4

**Détection du mensonge** : révèle les mensonges délibérés pendant 1 round/niveau ou jusqu’à utilisation.
**Liberté de mouvement** : la cible bouge normalement malgré les entraves pendant 10 minutes/ niveau.
**Neutralisation du poison** : élimine ou rend un poison inoffensif pendant 10 minutes/niveau.
**Restauration** : rend niveaux et points de caractéristique perdus.
**Soins intensifs** : rend 4d8 PV au sujet, +1/niveau (max. +20).
**Don des langues** : permet de parler toutes les langues pendant 10 minutes/niveau.

## Sorts divins de niveau 5

**Annulation d’enchantement** : libère la cible des enchantements, des altérations, des malédictions et de la pétrification.
**Colonne de feu** : feu divin infligeant 1d6 points de dégâts/niveau (max. 15d6).
**Communion** : le dieu du PJ répond par oui ou non à 1 question/niveau pendant 1 round/niveau.
**Rappel à la vie** : ressuscite quelqu’un mort depuis moins d’un jour/niveau.
**Soins légers de groupe** : rend 1d8 PV à de nombreuses créatures, +1/niveau (max. +25).
**Vision lucide** : permet de voir les choses telles qu’elles sont pendant 1 minute/niveau.

## Sorts divins de niveau 6

**Bannissement** : bannit 2 DV/niveau de créatures extérieures.
**Festin des héros** : nourriture pour 1 créature/niveau ; confère un bonus de +1 au combat pendant 12 heures. Perdure pendant 1 heure.
**Guérison suprême** : soigne 10 points de dégâts/niveau, les maladies et les troubles mentaux.
**Mise à mal** : inflige 10 points de dégâts/niveau. 
**Quête** : oblige une créature a effectuer une tâche.
Dure 1 jour/niveau ou jusqu’à la fin de la tâche. 
**Soins modérés de groupe** : rend 2d8 PV à de nombreuses créatures, +1/niveau (max. 30).

## Sorts divins de niveau 7

**Destruction** : tue la cible et détruit son corps. 
**Forme éthérée** : le PJ passe dans le plan Éthéré pour 1 round/niveau.
**Régénération** : fait repousser les membres tranchés, rend 4d8 points de vie, +1/niveau (max. +35). 
**Restauration suprême** : comme restauration, mais rend tous les niveaux et points de caractéristique perdus.
**Résurrection** : ramène un mort à la vie.
**Soins importants de groupe** : rend `3d8` PV à de nombreuses créatures, +1/niveau (max. +35).

## Sorts divins de niveau 8

**Aura sacrée** : +4 à la CA, bonus de résistance de +4 aux jets de sauvegarde et RM de 25 contre les sorts du Mal pendant 1 round/niveau.
**Localisation suprême** : localise précisément une créature ou un objet.
**Soins intensifs de groupe** : rend 4d8 PV à de nombreuses créatures, +1/niveau (max. +40).
**Tempête de feu** : inflige 1d6 points de dégâts de feu/niveau (max. `20d6`).
**Verrou dimensionnel** : téléportation et voyage entre les plans bloqués pendant 1 jour/niveau.
**Zone d’anti-magie** : réprime toute magie à moins de 3 m pendant 10 minutes/niveau.

## Sorts divins de niveau 9

**Capture d’âme** : retient l’âme d’un défunt pour empêcher sa résurrection.
**Guérison suprême de groupe** : comme guérison suprême, mais sur plusieurs sujets.
**Implosion** : tue 1 créature/round pendant 4 rounds. S’annule si la concentration est perdue.
**Passage dans l’éther** : emmène plusieurs créatures dans le plan Éthéré pendant 1 minute/niveau.
**Portail** : relie deux plans pour se déplacer ou appeler une entité pendant 1 round/niveau.
**Projection astrale** : emmène le PJ et ses compagnons dans le plan astral.
