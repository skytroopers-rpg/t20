# Magie druidique

Voilà la liste des sorts druidiques disponibles :

## Sorts druidiques de niveau 0

**Création d’eau** : crée 8 litres d’eau pure / niveau. 
**Détection de la magie** : détecte sorts et objets magiques à 18 m à la ronde pendant `1 minute / niveau`. 
**Détection du poison** : détecte le poison chez 1 créature ou 1 objet.
**Lecture de la magie** : permet de lire parchemins et livres de sorts pendant `10 minutes / niveau`. 
**Réparation** : répare sommairement un objet. 
**Résistance** : confère +1 aux jets de sauvegarde pendant 1 minute.

## Sorts druidiques de niveau 1

**Baie nourricière** : `2d4` baies rendant chacune 1 PV (max. 8 PV / 24 heures).
**Brume de dissimulation** : le PJ est entouré de brouillard pendant `1 minute / niveau`.
**Communication avec les animaux** : permet de communiquer avec les animaux pendant `1 minute / niveau`.
**Enchevêtrement** : la végétation immobilise tout dans un rayon de 12 m pendant `1 minute / niveau`.
**Flammes** : `1d6` points de dégâts, +1 / niveau, contact ou lancer.
**Lueur féerique** : illumine le sujet pendant `1 minute/niveau` (annule flou, camouflage, etc.).

## Sorts druidiques de niveau 2

**Bourrasque** : emporte ou renverse les créatures de taille modeste.
**Distorsion du bois** : tord le bois (manche d’arme, planche, porte, etc.) dans un rayon de 6 mètres. 
**Forme d’arbre** : transforme le PJ en arbre pendant 1 heure/niveau.
**Métal brûlant** : chauffe le métal et inflige des dégâts à qui le touche.
**Nuée grouillante** : convoque une nuée de chauves-souris, rats ou araignées pendant bonus d’ESP + niveau du lanceur + 2 rounds.
**Peau d’écorce** : confère un bonus d’armure naturelle de +2 (ou plus) pendant 10 minutes / niveau.

## Sorts druidiques de niveau 3

**Appel de la foudre** : appelle la foudre (3d6 points de dégâts par éclair) pendant `1 minute / niveau`. 
**Communication avec les plantes** : permet de communiquer avec les plantes et les créatures végétales pendant `1 minute / niveau`.
**Croissance d’épines** : `1d4` points de dégâts, lenteur possible pendant `1 heure/niveau` dans une zone de 1,5 mètres.
**Façonnage de la pierre** : permet de modeler la pierre. 
**Protection contre les énergies destructives** : absorbe `12 points de dégâts/niveau` infligés par le type d’énergie choisi pendant `10 minutes / niveau`. 
**Respiration aquatique** : permet de respirer sous l’eau pendant `2 heures / niveau`.

## Sorts druidiques de niveau 4

**Coquille antiplantes** : empêche les plantes animées d’approcher pendant `10 minutes / niveau` dans un rayon de 3 mètres.
**Marchedanslesairs** : le sujet marche dans les airs comme sur la terre ferme pendant `10 minutes / niveau`. 
**Pierres acérées** : `1d8` points de dégâts, lenteur possible dans une zone de 6 mètres.
**Réincarnation** : ramène le sujet à la vie, mais dans un autre corps.
**Tempête de grêle** : `5d6` points de dégâts dans un cylindre de 12 m de diamètre.
**Répulsif** : les insectes, araignées et autres vermines restent à 3 m du PJ pendant `10 minutes / niveau`.

## Sorts druidiques de niveau 5

**Appel de la tempête** : comme appel de la foudre, mais `5d6` points de dégâts par éclair.
**Éveil** : rend un animal ou un arbre intelligent. 
**Fléau d’insectes** : nuée de criquets attaquant des créatures pendant `1 minute / niveau`. 
**Métamorphose funeste** : transforme le sujet en animal inoffensif.
**Mur de feu** : `2d4` points de dégâts à moins de 3 m, `1d4` à moins de 6 m ; `2d6` points de dégâts, `+1 / niveau`, en cas de traversée.
**Transmutation de la pierre en boue** : affecte 2 cubes de 3 m d’arête par niveau.

## Sorts druidiques de niveau 6

**Chêne animé** : transforme un chêne en sylvanien pendant `1 jour/niveau`.
**Germes de feu** : glands et baies deviennent des projectiles ou des bombes (`1d4 dégâts / niveau`, 4 graines) dans un rayon de 3 mètres.
**Glissement de terrain** : fait apparaître tranchées ou collines jusqu’à 225 mètres carrés de surface et jusqu’à 6 mètres de haut ou de profondeur.
**Pierres commères** : permet de communiquer avec la pierre pendant `1 minute / niveau`.
**Voie végétale** : le PJ entre dans une plante et ressort par une autre.
**Mur de pierre** : crée un mur qui peut être façonné. 

## Sorts druidiques de niveau 7

**Bâton sylvanien** : transforme le bâton du PJ en sylvanien.
**Contrôle du climat** : modifie le climat local. 
**Tempête de feu** : inflige `1d6` points de dégâts de feu / niveau.
**Vent divin** : transforme les cibles en vapeur et les emporte rapidement à 90 km/h pendant `1 heure/niveau`.
**Tremblement de terre** : secousse sismique dans un rayon de 24 m.
**Doigt de mort** : tue la cible.
