# Combat

Parce que, dans certains cas, la confrontation est inévitable, voilà comment se déroule un combat :

1. On définit l'ordre de jeu des joueurs pour l'ensemble de la bataille
2. Chaque personne joue son tour, l'une après l'autre
3. On recommence une fois que tout le monde a joué et si le combat n'est pas terminé

*À noter* : un tour représente environ 6 secondes dans la "vraie vie" donc ne pensez pas faire de choses démesurées en un seul tour !

## Ordre de jeu ou Initiative

Pour déterminer l'ordre de jeu dans un combat, chaque personnage fait un *jet d'initiative* pour savoir quand il•elle jouera. 

Le jet est le suivant : `1d20 + Bonus de DEX`.

On joue ensuite par ordre d'initiative décroissant.

## Action

Lors d'un combat, un personnage peut se déplacer et faire une seule action par tour :

- attaquer
- lancer un sort
- aider un allié
- esquiver
- foncer
- etc.

## Déplacement

Vous vous déplacez dans la limite du raisonnable (le•a *Maître•sse du Jeu* vous aidera si besoin). N'imaginez pas que vous allez pouvoir traverser une grande pièce en un tour, ni monter 429 marches d'un escalier.

## Attaque

L'attaque se passe en 3 phases :

1. Vous tentez de porter un coup
2. Le•a *Maître•sse du Jeu* me dit si vous avez réussi
2. Si vous touchez, vous calculez le nombre de blessures que vous avez infligé à votre adversaire

### Porter un coup

Pour porter un coup, vous devez faire un *Jet d'Attaque* (`d20`). Ajoutez ensuite un bonus d'attaque en fonction du type d'attaque que vous faites :

- attaque au corps à corps : `niveau + Bonus de FOR`
- attaque à distance : `niveau + Bonus de DEX`
- attaque magique : `niveau + Bonus d'ESP`

Par exemple, [Kendric](../joueur/personnages-pretires.md) (guerrier de niveau 1 avec +3 en bonus de force) ajoutera un bonus de `+4` à son `d20`.

Un•e attaquant•e peut décider d'utiliser son `bonus de DEX + niveau` comme bonus d’attaque au corps-à-corps, s’il•elle utilise une arme légère. 

Les *guerrièr•e•s*, *voleur•euse•s* et *rôdeur•euse•s* peuvent aussi porter deux armes légères et attaquer avec les deux à la fois au prix d’un malus de `-2` à tous les jets d’attaque lors de ce tour.

### Toucher un adversaire

Pour toucher un adversaire (ou si un adversaire vous attaque), il faut comparer votre *Jet d'Attaque* à sa *Classe d'Armure* (ou *CA*). S'il est supérieur, le coup a porté. 

La *Classe d'Armure* (ou *CA*) est égale à `10 + Bonus de DEX` si vous ne portez pas d'armure. Sinon, elle correspond à la [CA de votre armure](../equipement/armures.md).

Dans le cas où vous feriez un `20 naturel` (Le dé indique 20 avant que vous ajoutiez les bonus), vous touchez obligatoirement et les dégâts sont doublés.

## Dégâts

Ça y est, vous avez touché votre adversaire. Vous pouvez maintenant lancer un *Jet de Dégâts*. Il dépend de l'arme que vous avez utilisé :

- au corps à corps : `dégâts de l'arme + Bonus de FOR` (`Bonus de FOR x 2` pour les armes à deux mains)
- à distance : `dégâts de l'arme + Bonus de DEX`

## Inconscience et mort

Lorsque les points de vie d'un personnage sont réduits à 0, il tombe dans l'inconscience et se rapproche de la mort. Les dommages supplémentaires diminuent la *FOR*. Si celle-ci atteint 0, le personnage meurt.

Une fois le combat terminé, si le personnage a perdu des points de *FOR*, il peut les regagner mais il lui faudra du temps. Il peut en récupérer un peu (`1d4 + Bonus de FOR avant inconscience`) après chaque nuit de repos.

## Attaques à la chaîne

> Avec l'expérience, les héros apprennent à faire plusieurs attaques par tour.

Si le bonus total du personnage est supérieur ou égal à `+6`, il peut accomplir une seconde attaque avec un malus de `-5`. Si son bonus total est de `+11` ou plus, il peut tenter une troisième attaque à `-10`. 

Par exemple, un guerrier de niveau 2 avec un bonus de FOR de `+6` pourra faire 2 attaques avec comme bonus : `+8 et +3`. Un rôdeur de niveau 8 avec un bonus de DEX de `+4` pourra faire trois attaques avec des bonus de `+12, +7 et +2`.
