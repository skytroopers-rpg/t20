# Montures

Voici une liste *non-exhaustive* des montures disponibles, ainsi que de leurs équipements.

| Article                                      | Prix    |
|----------------------------------------------|---------|
| Âne ou mulet                                 | 8 PO    |
| Barde, Créature de taille moyenne            | 700 PO  |
| Barde, Créature de grande taille             | 1500 PO |
| Destrier léger (Cheval)                      | 150 PO  |
| Destrier lourd (Cheval)                      | 400 PO  |
| Cheval léger                                 | 75 PO   |
| Cheval lourd                                 | 200 PO  |
| Poney                                        | 30 PO   |
| Poney de guerre                              | 100 PO  |
| Chien de garde                               | 25 PO   |
| Chien de selle                               | 150 PO  |
| Écurie (pour une journée)                    | 5 PA    |
| Fontes                                       | 4 PO    |
| Mors et bride                                | 2 PO    |
| Nourriture pour animaux (par jour)           | 5 PC    |
| Selle d'équitation                           | 10 PO   |
| Selle d'équitation spéciale                  | 30 PO   |
| Selle de bât                                 | 5 PO    |
| Selle de bât spéciale                        | 15 PO   |
| Selle de guerre                              | 20 PO   |
| Selle de guerre spéciale                     | 60 PO   |
