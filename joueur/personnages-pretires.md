# Personnages pré-tirés

## 🗡 Fey, Voleuse humaine, niveau 1

| Caractéristiques et équipement                    | Compétences      |
|---------------------------------------------------|------------------|
| 💪 FOR 12 (+1) - DEX 15 (+2) - ESP 12 (+1)        | Physique +2      |
| ❤️ PV 18 - PM 3                                   | Subterfuge +5    |
| 🛡 CA 12 : armure de cuir                         | Connaissance +2  |
| ⚔️ Équipement : deux épées courtes                | Communication +2 |
| 🩸 Attaque / dégâts : `1d20 + 2` / `1d6 + 1`      | Survie +2        |

## ⚔️ Kendric, Guerrier nain, niveau 1

| Caractéristiques et équipement                    | Compétences      |
|---------------------------------------------------|------------------|
| 💪 FOR 16 (+3) - DEX 13 (+1) - ESP 11 (0)         | Physique +4      |
| ❤️ PV 24 - PM 2                                   | Subterfuge +1    |
| 🛡 CA 17 : cotte de mailles et rondache en acier  | Connaissance +1  |
| ⚔️ Équipement : épée longue                       | Communication +1 |
| 🩸 Attaque / dégâts : `1d20 + 5` / `1d8 + 3`      | Survie +1        |

## 🔥 Chandra, Mage Haute-elfe, niveau 1

| Caractéristiques et équipement                    | Compétences      |
|---------------------------------------------------|------------------|
| 💪 FOR 11 (+1) - DEX 13 (+1) - ESP 17 (+3)        | Physique +1      |
| ❤️ PV 17 - PM 5                                   | Subterfuge +1    |
| 🛡 CA 12 : Armure de cuir                         | Connaissance +4  |
| ⚔️ Équipement : bâton                             | Communication +1 |
| 🩸 Attaque / dégâts : `1d20 + 1` / `1d6 + 1`      | Survie +1        |
| ✨ Magie : sorts profanes de niveau 0 et 1        |                  |

## ⛑ Barnabas, Prêtre halfelin, niveau 1

| Caractéristiques et équipement                    | Compétences      |
|---------------------------------------------------|------------------|
| 💪 FOR 10 (0) - DEX 16 (+3) - ESP 13 (+1)         | Physique +1      |
| ❤️ PV 15 - PM 3                                   | Subterfuge +1    |
| 🛡 CA 16 : Cotte de mailles                       | Connaissance +1  |
| ⚔️ Équipement : Morgenstern                       | Communication +4 |
| 🩸 Attaque / dégâts : `1d20 + 1` / `1d8`          | Survie +1        |
| ✨ Magie : sorts divins de niveau 0 et 1          |                  |

## 🙌 Elroze, Demi-orque paladine, niveau 1

| Caractéristiques et équipement                    | Compétences      |
|---------------------------------------------------|------------------|
| 💪 FOR 18 (+4) - DEX 12 (+1) - ESP 11 (0)         | Physique +2      |
| ❤️ PV 27 - PM 2                                   | Subterfuge +1    |
| 🛡 CA 17 : Cotte de mailles et rondache en acier  | Connaissance +1  |
| ⚔️ Équipement : Épée longue                       | Communication +3 |
| 🩸 Attaque / dégâts : `1d20 + 5` / `1d8 + 4`      | Survie +1        |
 
## 🏹 Rundrin, Drakéide rôdeur, niveau 1

| Caractéristiques et équipement                    | Compétences      |
|---------------------------------------------------|------------------|
| 💪 FOR 14 (+2) - DEX 16 (+3) - ESP 10 (0)         | Physique +1      |
| ❤️ PV 21 - PM 2                                   | Subterfuge +1    |
| 🛡 CA 16 : Armure de cuir                         | Connaissance +1  |
| ⚔️ Équipement : Arc long                          | Communication +1 |
| 🩸 Attaque / dégâts : `1d20 + 4` / `1d8 + 3`      | Survie +4        |

## ✨ Clellnis, Gnome illusionniste, niveau 1

| Caractéristiques et équipement                    | Compétences      |
|---------------------------------------------------|------------------|
| 💪 FOR 11 (0) - DEX 14 (+2) - ESP 15 (+3)         | Physique +1      |
| ❤️ PV 16 - PM 5                                   | Subterfuge +2    |
| 🛡 CA 12 : Aucune armure                          | Connaissance +1  |
| ⚔️ Équipement : Dague                             | Communication +3 |
| 🩸 Attaque / dégâts : `1d20 + 3` / `1d4 + 2`      | Survie +1        |
| ✨ Magie : sorts d'illusions de niveau 0 et 1     |                  |

## ☘️ Shelara, Druidesse Elfe Sylvaine, niveau 1

| Caractéristiques et équipement                    | Compétences      |
|---------------------------------------------------|------------------|
| 💪 FOR 10 (0) - DEX 14 (+2) - ESP 17 (+3)         | Physique +1      |
| ❤️ PV 15 - PM 5                                   | Subterfuge +1    |
| 🛡 CA 13 : Armure de cuir                         | Connaissance +3  |
| ⚔️ Équipement : Bâton                             | Communication +1 |
| 🩸 Attaque / dégâts : `1d20` / `1d6`              | Survie +3        |
| ✨ Magie : sorts druidiques de niveau 0 et 1      |                  |

## 🎶 Berdil, Barde Demi-elfe, niveau 1

| Caractéristiques et équipement                    | Compétences      |
|---------------------------------------------------|------------------|
| 💪 FOR 12 (+1) - DEX 15 (+2) - ESP 13 (+1)        | Physique +1      |
| ❤️ PV 18 - PM 3                                   | Subterfuge +2    |
| 🛡 CA 13 : Armure de cuir                         | Connaissance +1  |
| ⚔️ Équipement : Épée courte                       | Communication +2 |
| 🩸 Attaque / dégâts : `1d20 + 3` / `1d6 + 2`      | Survie +1        |
