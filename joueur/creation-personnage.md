# Création de personnage

Vous souhaitez créer votre personnage ? Vous êtes au bon endroit !

Voici les quelques étapes à suivre pour avoir un personnage complet :

1. Choisir sa classe
2. Choisir sa race
3. Connaître ses caractéristiques et ses compétences
4. Penser à son passé
5. Choisir son équipement
6. Trouver un nom (souvent, l'étape la plus longue ^^)

## Choisir sa classe

Alors que certains jeux de rôles commencent par la race du personnage, nous allons nous concentrer sur le type 
de personnage que nous voulons jouer avant ça.

Voici un récapitulatif des classes disponibles :

- ⚔️ **Guerrier•ère** : J'ai un physique hors du commun. Je me bat avec une armure et un bouclier et je tape fort.
- 🗡 **Voleur•euse** : Je suis fourbe et rapide. Je me faufile et j'attaque mes ennemis sournoisement. 
- 🔥 **Mage** : Je suis érudit•e. Je lance des sorts profanes, de la boule de feu à la lévitation, en passant par la manipulation d'objets à distance.
- ⛑ **Prêtre•sse** : Je communique bien, avec mes alliés comme avec les dieux. Je peux lancer des sorts divins et repousser les mot vivants.
- 🙌 **Paladin•e** : Je suis animé•e par la volonté de faire le bien sur cette terre, par la force ou par le soin, et je suis immunisé aux maladies.
- 🏹 **Rôdeur•euse** : Je sais survivre dans la nature et je suis imbattable lors qu'il s'agit de pister des créatures ou attaquer à distance.
- ✨ **Illusionniste** : Je suis passé•e maître•sse dans l'art de la manipulation et des illusions. Je peux même me transformer ou disparaître.
- ☘️ **Druide•sse** : Je sais survivre dans la nature et je l'utilise pour arriver à mes fins. Je peux aussi me transformer en animal plusieurs fois par jour.
- 🎶 **Barde•sse** : Je chante/joue de la musique pour aider mes allié•e•s dans leurs prouesses. Je peux aussi rendre mes ennemis fous.

La présentation détaillée des classes est disponible dans [une page dédiée](./classes.md).

Une fois votre classe choisie, passons à la race, qui sera (de préférence) adaptée à la classe choisie. 
Il y a peu de chances de croiser un demi-orque mage, ou un homme-sylvestre guerrier, même si ça existe.

## Choisir sa race

Voici un récapitulatif des races disponibles :

- Les **humains** sont plutôt polyvalents.
- Les **haut-elfes** sont intelligents.
- Les **nains** sont forts.
- Les **halfelins** sont agiles.
- Les **gnomes** sont un peu plus agiles et intelligents que la moyenne.
- Les **demi-orques** sont très forts, mais peu intelligents.
- Les **demi-elfes** sont agiles et un peu polyvalents.
- Les **drakéides** sont forts et agiles, mais peu intelligents.
- Les **elfes sylvains** sont agiles et intelligents, mais plutôt faibles.

La présentation détaillée des races est disponible dans [une page dédiée](./races.md).

Une fois votre race choisie, nous allons tirer vos caractéristiques.

## Caractéristiques

Les **caractéristiques** représentent les *capacités physiques et mentales* de votre personnage (ou de toute créature d'ailleurs). Il y en a seulement 3 : 

- la **Force** (FOR), qui représente la puissance physique et la constitution de votre personnage
- la **Dextérité** (DEX), qui correspond à son agilité et ses réflexes
- l'**Esprit** (ESP), qui symbolise sa volonté, sa perception et son intuition

### Calcul des caractéristiques

Pour calculer vos caractéristiques, procédez de la manière suivante :
Lancez `2d6 + 6` et notez le résultat. 
Une fois les 3 résultats notés, allouez chacun d'entre eux à une caractéristique.

> Si vous êtes phobique des jets de dés, et avec l'accord de votre *Maître•sse de Jeu*, vous pouvez choisir de ne pas jeter de dés et de répartir un total de 40 points dans les 3 caractéristiques.

Modifiez ensuite vos caractéristiques avec vos [bonus de race](./races.md)... et c'est fini !

À noter : vous gagnez `1 point` de caractéristique à chaque niveau pair que vous atteignez.

### Bonus de caractéristique

Le bonus est calculé de la manière suivante : `(Caractéristique – 10) / 2, arrondi à l’inférieur`.

Tableau récapitulatif des bonus : 

| Caractéristique | Bonus |
|-----------------|-------|
| 0-1             | -5    |
| 2-3             | -4    |
| 4-5             | -3    |
| 6-7             | -2    |
| 8-9             | -1    |
| 10-11           | 0     |
| 12-13           | +1    |
| 14-15           | +2    |
| 16-17           | +3    |
| 18-19           | +4    |
| 20-21           | +5    |
| 22-23           | +6    |
| 24-25           | +7    |
| 26-27           | +8    |
| 28-29           | +9    |
| 30-31           | +10   |

## Points de vie et de magie

Les points de vie sont calculés de la manière suivante : `Caractéristique de FOR + Bonus de FOR + 5`. À chaque montée de niveau, vous gagnez `Bonus de FOR + 5` points de vie supplémentaires.

Les points de magie, ou mana, sont calculés de la manière suivante : `Bonus d'ESP +2`.
À chaque montée de niveau, vous gagnez `Bonus d'ESP + 2` points de magie supplémentaires.

## Compétences

Lorsque vous souhaitez, ou que vous devez, faire une action, le•a *Maître•sse du Jeu* pourra vous demander de faire un test de compétence. Il en existe 5 : *Physique*, *Subterfuge*, *Connaissance*, *Communication* et *Survie*. 

Voici des exemples d'actions par compétence :

- *Physique* : escalader, nager, sauter, pousser / soulever
- *Subterfuge* : éviter la chute d'un objet, se déplacer silencieusement, crocheter une serrure
- *Connaissance* : fouiller, décrypter, estimer, utiliser des cordes
- *Communication* : Se déguiser, intimider, bluffer, se renseigner, dresser un animal
- *Survie* : pister, chasser, s'orienter, poser/détecter des pièges, rechercher de l'eau ou des herbes (seuls les *rôdeurs* et les *druides* peuvent utiliser cette dernière compétence).

> À noter : vous gagnez `1 point` pour chaque compétence à chaque niveau impair que vous atteignez. 

Vous commencez donc avec une base de `1 point` par compétence. Ajoutez ensuite vos bonus de [race](./races.md) et de [classe](./classes.md).

## Penser à son passé

Maintenant que vous avez votre personnage, avec ses caractéristiques et ses compétences, il faut penser à *son histoire*. Vous n'êtes pas obligé•e d'écrire un roman, quelques lignes suffiront pour commencer. 

Décrivez succintement son apparence, son comportement et ses objectifs dans la vie. Cherche-t-il•elle simplement à voyager ou à s'enrichir ? Est-il•elle animé•e par une mission ? Cache-t-il•elle un secret ? 

Vous pourrez bien évidemment modifier cette histoire au fur et à mesure que vous avancerez dans la partie. 

*À noter* : ces informations sont, par défaut, purement liée à votre rôle ([Voir la partie "Jeu de rôle"](./aventure.md)) mais le• *Maître•sse du Jeu* peut vous accorder certaines modifications (voire des bonus). Tout est une question de RP et de négociation.

## Matériel d’aventurier

Maintenant que vous connaissez l'histoire de votre personnage, vous pouvez choisir son équipement. Chaque aventurier commence avec `3d10` pièces d’or et un des paquets de départ ci-dessous.

Choisissez un paquet ou lancez `1d6` pour en choisir un au hasard.
Chaque paquet contient :

- Sac à dos
- sacoche de ceinture
- couverture
- outre
- rations de voyage (4 jours)

### Paquet A (1-2)

- lanterne à capote
- 10 flasques d’huile
- silex et amorce
- pelle
- 2 chausses-trappes
- gourde

### Paquet B (3-4)

- 10 torches
- 3 flasques d’huile
- 10 pièces de craie
- perche de 3 m
- miroir
- pied-de-biche
- gourde d’eau.

### Paquet C (5-6)

- tente
- 10 torches
- 5 flasques d’huile
- silex et amorce
- corde en chanvre de 15 m
- grappin
- perche de 3 m.

### Équipement supplémentaire

En fonction de votre classe, vous avez un petit quelque chose en plus :

- **Guerrier•ère** : une chevalière
- **Voleur•euse** : des outils de cambrioleur
- **Mage** : un grimoire et une sacoche à composantes
- **Prêtre•sse** : un symbole sacré en argent
- **Paladin•e** : une flasque d’eau bénite
- **Rôdeur•euse** : une pierre à tonnerre
- **Illusionniste** : un jeu de dés en os/de cartes
- **Druide•sse** : un symbole sacré en bois
- **Barde•sse** : un instrument de musique

## Trouver un nom

Maintenant que "le plus simple" est fait, le plus difficile arrive (pour la majorité des joueurs•euses) : il faut trouver un nom à votre personnage. Vous pouvez avoir un pseudo ou un prénom avec nom, avec ou sans particule, bref, tout est permis.

Le principal est que vous vous sentiez à l'aise avec parce que le• *Maître•sse du Jeu* va vous appeler par ce nom beaucoup plus souvent que vous ne le pensez.

## C'est parti pour l'aventure !

Et voilà, vous êtes prêt•e pour partir à l'aventure. Laissez vous guider et tout ira bien. Un dernier conseil pour la route : `le travail d'équipe et la collaboration améliorent grandement les chances de votre groupe de survivre aux nombreux périls`\*.

Maintenant que vous avez votre personnage, vous pouvez passer à la partie "[Aventures](../regles/aventure.md)".

\* Citation [AideDD](https://www.aidedd.org/regles/creation-de-perso/)
